/* main.c
 *
 * Copyright 2024 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <glib.h>
#include <gudev/gudev.h>
#include <stdlib.h>

static void
print_device (GUdevDevice *device)
{
  const char * const *keys = g_udev_device_get_property_keys (device);
  const char * const *sysfs_attrs = g_udev_device_get_sysfs_attr_keys (device);

  if (!g_udev_device_get_device_file (device))
    return;

  g_print ("\n");
  if (g_udev_device_get_action (device))
    g_print ("[%s] %s\n", g_udev_device_get_action (device), g_udev_device_get_sysfs_path (device));
  else
    g_print ("%s\n", g_udev_device_get_sysfs_path (device));
  g_print ("============================\n");
  g_print ("    driver: %s\n", g_udev_device_get_driver (device));
  g_print ("    subsystem: %s\n", g_udev_device_get_subsystem (device));
  g_print ("    device file: %s\n", g_udev_device_get_device_file (device));
  g_print ("    initialized: %d\n", g_udev_device_get_is_initialized (device));

  g_print ("    properties:\n");
  for (size_t i = 0; keys && keys[i]; i++)
    g_print ("        %s: %s\n", keys[i], g_udev_device_get_property (device, keys[i]));

  g_print ("    sysfs attrs:\n");
  for (size_t i = 0; sysfs_attrs && sysfs_attrs[i]; i++)
    g_print ("        %s: %s\n", sysfs_attrs[i], g_udev_device_get_sysfs_attr (device, sysfs_attrs[i]));

  g_print ("============================\n");
}

static void
on_gudev_client_uevent_cb (GUdevClient *client,
                           const char  *action,
                           GUdevDevice *device,
                           gpointer user_data)
{
  print_device (device);
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GUdevClient) client = NULL;
  g_autoptr(GPtrArray) subsystems = NULL;
  g_autoptr(GMainLoop) mainloop = NULL;
  g_autoptr(GError) error = NULL;
  gboolean version = FALSE;
  GOptionEntry main_entries[] = {
    { "version", 0, 0, G_OPTION_ARG_NONE, &version, "Show program version" },
    { NULL }
  };

  context = g_option_context_new ("- test program for GUdev inside a Flatpak sandbox");
  g_option_context_add_main_entries (context, main_entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (version)
    {
      g_printerr ("%s\n", PACKAGE_VERSION);
      return EXIT_SUCCESS;
    }

  mainloop = g_main_loop_new (NULL, FALSE);

  subsystems = g_ptr_array_new_full (2, g_free);
  for (int i = 1; i < argc; i++)
    g_ptr_array_add (subsystems, g_strdup (argv[i]));
  g_ptr_array_add (subsystems, NULL);

  client = g_udev_client_new ((const char * const *) subsystems->pdata);
  g_signal_connect (client,
                    "uevent",
                    G_CALLBACK (on_gudev_client_uevent_cb),
                    NULL);

  if (argc > 1)
    {
      for (int i = 1; i < argc; i++)
        {
          g_autolist(GUdevDevice) devices = NULL;

          devices = g_udev_client_query_by_subsystem (client, argv[i]);
          for (GList *l = devices; l; l = l->next)
            print_device (l->data);
        }
    }
  else
    {
      g_autolist(GUdevDevice) devices = NULL;

      devices = g_udev_client_query_by_subsystem (client, NULL);
      for (GList *l = devices; l; l = l->next)
        print_device (l->data);
    }

  g_main_loop_run (mainloop);

  return EXIT_SUCCESS;
}
